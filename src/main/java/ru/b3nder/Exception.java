package ru.b3nder;

public class Exception {
    public static void arithmeticException() {
        int a = 10 / 0;
    }

    public static void arrayIndexOutOfBoundsException() {
        int[] arr = new int[10];
        arr[15] = 10;
    }

    public static void numberFormatException() {
        int a = Integer.parseInt("Number");
    }

    public static void stringIndexOutOfBoundsException() {
        String str = "Hello";
        char ch = str.charAt(8);
    }

    public static void nullPointerException() {
        String str = null;
        str.hashCode();
    }

    public static void classCastException() {
        Object num = 0;
        String str = (String) num;
    }

    public void stackOverFlowError(int num) {
        if (num == 0) {
            return;
        } else {
            stackOverFlowError(++num);
        }
    }
}

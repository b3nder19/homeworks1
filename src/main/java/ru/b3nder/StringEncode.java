package ru.b3nder;

public class StringEncode {
    public String encodeStr(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            int count = 1;
            while (i < str.length() - 1 && str.charAt(i) == str.charAt(i + 1)) {
                count++;
                i++;
            }
            sb.append(count).append(str.charAt(i));
        }
        return sb.toString();
    }
}

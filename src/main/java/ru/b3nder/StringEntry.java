package ru.b3nder;

public class StringEntry {
    public int entry(String str, String userStr) {
        if (str.equals("") || userStr.equals("")) {
            return 0;
        } else {
            return ((str.length() - str.replaceAll(userStr, "").length()) / userStr.length());
        }
    }
}

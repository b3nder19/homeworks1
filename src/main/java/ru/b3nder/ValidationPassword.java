package ru.b3nder;

import java.util.Scanner;
import java.util.regex.Pattern;

public class ValidationPassword {
    public boolean password(String password) {
        boolean result = Pattern.matches("(?=.*[a-z])(?=.*[A-Z])(?=.*[\\d])[a-zA-Z\\d]{8,100}", password);
        return result;
    }
}

package ru.b3nder;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class TestValidationPassword {
    @Test
    public void testPassword() {
        ValidationPassword validationPassword = new ValidationPassword();
        assertTrue(validationPassword.password("asdasd1123V"));
        assertTrue(validationPassword.password("123aaVV1"));
        assertFalse(validationPassword.password("123aaVV"));
        assertFalse(validationPassword.password("1234567a"));
        assertFalse(validationPassword.password("passworD123!"));
    }
}

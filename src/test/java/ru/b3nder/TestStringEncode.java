package ru.b3nder;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class TestStringEncode {
    @Test
    public void testEncode() {
        StringEncode stringEncode = new StringEncode();

        String str1 = stringEncode.encodeStr("aabbcddd");
        String str2 = stringEncode.encodeStr("abbbccccdd");

        assertEquals("2a2b1c3d", str1);
        assertEquals("1a3b4c2d", str2);
    }
}

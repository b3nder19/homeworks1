package ru.b3nder;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class ExceptionsTest {
    @Test
    public void arithmeticException() {
        assertThrows(ArithmeticException.class, Exception::arithmeticException);
    }

    @Test
    public void testArrayIndexOutOfBoundsException() {
        assertThrows(ArrayIndexOutOfBoundsException.class, Exception::arrayIndexOutOfBoundsException);
    }

    @Test
    public void testNumberFormatException() {
        assertThrows(NumberFormatException.class, Exception::numberFormatException);
    }

    @Test
    public void testStringIndexOutOfBoundsException() {
        assertThrows(StringIndexOutOfBoundsException.class, Exception::stringIndexOutOfBoundsException);
    }

    @Test
    public void testNullPointerException() {
        assertThrows(NullPointerException.class, Exception::nullPointerException);
    }

    @Test
    public void testClassCastException() {
        assertThrows(ClassCastException.class, Exception::classCastException);
    }

    @Test()
    public void testStackOverFlowError() {
        Exception exception = new Exception();
        exception.stackOverFlowError(-1);
    }
}
